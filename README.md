# Workshop on Monitoring

Prometheus, Prometheus alert manager, metrics & labels, k8s discovery, Prometheus Federations and Anodot

## Metrics

Metric is a momentary named value that represent current state of the systems.

There are 4 types of metrics:

### Counter

Used for number of requests, completed tasks - anything that monotonically increases.

* Starts with zero
* Increases

### Gauge

"Counter", that can go both directions - temperature, mem usage, CPU load etc

### Histogram

Counters placed in "buckets". I.e. normal counter name is *request_count*, but we want to have them sorted by duration of request, so histogram makes a set of metrics with respective labels:

*request_count_sum* - total sum of requests duration

*request_count_bucket{le="200"}* - count of requests below 200ms

*request_count_count* - equalt to *request_count_bucket{le="+Inf"}* - total count of requests

### Summary

"Sophisticated" histogram :-)

## Instrumentation

Using client library, software developer exposes the metrics at `/metrics` of hist software, network team ensures connectivity Prometheus -> Software.

